import asyncio
from dataclasses import dataclass
from typing import Any, Callable, Coroutine, TypeVar
from datetime import datetime, timedelta
import yaml

DTCL_CNF = {"frozen": True}

R = TypeVar("R")


# PEP 613 not supported by mypy version 0.910 yet
# TypeProject: TypeAlias = "Project"
# TypeInstance: TypeAlias = "Instance"
# TypeImage: TypeAlias = "Image"
# TypeMinecraft: TypeAlias = "MinecrafServer"

# TODO Make up my mind on wheter I want cmd to be run be passed as a whitespace separated string or a tuple of stings


async def run(
    cmd: list[str],
    error_handler: Callable[[str], Coroutine[Any, Any, str]] | None = None,
) -> str:
    proc = await asyncio.create_subprocess_shell(
        " ".join(cmd), stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    """
    Runs a shell command, and if provided lets the error_handler coroutine return a value in case of an error.
    """

    stdout, stderr = await proc.communicate()

    if stderr:
        if error_handler:
            return await error_handler(stderr.decode())
        raise Exception(f"Error while running command: {cmd}", stderr.decode())

    return stdout.decode()


async def co_run(*coros: Coroutine[Any, Any, R]) -> list[R]:
    """
    Runs all coroutines concurrently and awaits resutls.
    """
    tasks = (asyncio.create_task(coro) for coro in coros)
    return await asyncio.gather(*tasks)


@dataclass(**DTCL_CNF)
class Project:
    id: str
    zone: str

    async def get_instances(self):
        pass

    async def get_images(self):
        pass

    async def create_image(
        self, instance: "Instance", img_id: str, img_loc: str
    ) -> "Image":
        stdout = await run(
            [
                "gcloud compute machine-images create",
                img_id,
                f"--project={self.id}",
                f"source-instanc={instance.id}",
                f"--source-instance-zone={self.zone}",
                f"--storage-location={img_loc}",
            ]
        )

        print(stdout)
        return Image(img_id, self, img_loc)

    # TODO Method to create an instance out of an image


@dataclass(**DTCL_CNF)
class Image:
    id: str
    project: "Project"
    location: str


@dataclass(**DTCL_CNF)
class Instance:
    id: str
    project: "Project"
    user: str
    path: str

    async def status(self) -> dict:
        stdout = await run(
            [
                "gcloud compute instances describe",
                f"--project={self.project.id}",
                f"--zone={self.project.zone}",
                self.id,
            ]
        )

        return yaml.safe_load(stdout)

    async def running(self) -> bool:
        status = await self.status()
        return status["status"] == "RUNNING"

    # Instance status on glcoud does not get updated instantly after running the shutdown method
    async def safe_ip(self, ip: str | None = None) -> str | None:
        if ip is not None:
            return ip
        status = await self.status()
        if status["status"] == "RUNNING":
            return status["networkInterfaces"][0]["accessConfigs"][0]["natIP"]
        else:
            return None

    async def ssh_run(
        self,
        cmd: str,
        ip: str | None = None,
        error_handler: Callable[[str], Coroutine[Any, Any, str]] | None = None,
    ) -> str:
        ip = await self.safe_ip(ip)
        if ip is None:
            return "Instance is offline"
        stdout = await run(["ssh", f"{self.user}@{ip}", f"'{cmd}'"], error_handler)
        return stdout.strip()

    async def get_mc_pid(self, ip: str | None = None) -> str:
        pid = await self.ssh_run(f"cat {self.path}/server.pid", ip)
        return pid

    async def kill_mc(self, ip: str | None = None, pid: str | None = None) -> str:
        ip = await self.safe_ip(ip)
        if ip is None:
            return "Instance is offline"
        if pid is None:
            pid = await self.get_mc_pid(ip)
        stdout = await self.ssh_run(f"kill -14 {pid}", ip)
        return stdout

    async def get_mc_server(self, rcon_pass: str, rcon_retry_limit: int, ip=None):
        """
        Creates a MinecrafServer instance if there's one running.
        Otherwise returns None.
        """
        ip = await self.safe_ip(ip)
        if ip is None:
            return None
        pid = await self.get_mc_pid(ip)
        pid_status = await self.ssh_run(f"ps -p {pid}", ip)
        if "java" not in pid_status:
            return None
        return MinecrafServer(pid, self, rcon_pass, rcon_retry_limit)

    async def start_mc_server(self, rcon_pass: str, rcon_retry_limit: int, ip=None):
        """
        Starts the minecraft server process if it's not already running,
        and returns its MinecrafServer dataclass.
        """
        ip = await self.safe_ip(ip)
        if ip is None:
            return None
        pid = await self.get_mc_pid(ip)
        pid_status = await self.ssh_run(f"ps -p {pid}", ip)
        if "java" not in pid_status:
            await self.ssh_run(f"cd {self.path} && ./run.sh &>/dev/null &", ip)
            pid = await self.get_mc_pid(ip)
        return MinecrafServer(pid, self, rcon_pass, rcon_retry_limit)

    async def shutdown(self, ip: str | None = None) -> str:
        if not await self.running():
            return "Instance already shutdown"

        async def error_handler(err: str) -> str:
            if "closed by remote host" in err:
                return err.strip()
            else:
                raise Exception("Could not shutdown instance", err)

        stdout = await self.ssh_run(f"sudo shutdown now", ip, error_handler)
        return stdout.strip()

    async def start(self, ip: str | None = None) -> str:

        if await self.running():
            return "Instance already running"

        async def error_handler(err: str) -> str:
            if "done" in err:
                return err.strip()
            else:
                raise Exception("Could not start instance", err)

        stdout = await run(
            [
                "gcloud compute instances start",
                self.id,
                f"--zone={self.project.zone}",
            ],
            error_handler,
        )
        return stdout.strip()

    # TODO: Functions for setting up new minecraft server configurations and switching between them

    # TODO: Functions for configuring the instance enviroment to allow running the minecraft server


@dataclass(**DTCL_CNF)
class MinecrafServer:
    pid: str
    host: "Instance"
    rcon_pass: str
    rcon_retry_limit: int

    async def send_rcon(self, cmd: str, ip: str | None = None, retry: int = 0) -> str:

        ip = await self.host.safe_ip()
        if ip is None:
            return "Instance is offline"

        if await self.alive(ip):

            async def error_handler(err: str) -> str:
                if retry < self.rcon_retry_limit:
                    print("Server alive and not responding. Retrying...")
                    await asyncio.sleep(10)
                    return await self.send_rcon(cmd, ip, retry + 1)
                else:
                    raise Exception("Can't communicate with rcon", err)

            stdout = await run(
                ["mcrcon -H", ip, "-p", self.rcon_pass, cmd], error_handler
            )
            return stdout
        else:
            return "Server is not running"

    async def alive(self, ip: str | None = None) -> bool:
        pid_status = await self.host.ssh_run(f"ps -p {self.pid}", ip)
        return "java" in pid_status

    async def empty_time(self, ip: str | None = None) -> timedelta | str:

        ip = await self.host.safe_ip()
        if ip is None:
            return "Instance is offline"

        tasks = co_run(
            self.send_rcon("list", ip=ip),
            self.host.ssh_run(f"cat {self.host.path}/logs/latest.log", ip),
            self.host.ssh_run("date +'%H:%M:%S'", ip),
        )
        online, log, server_response = await tasks
        server_time = datetime.strptime(server_response.strip(), "%H:%M:%S")
        if online.split()[2] != "0":
            return timedelta()
        else:
            disconnects: list[str] = [
                *filter(lambda x: "left the game" in x, slog := log.split("\n"))
            ]
            if len(disconnects) == 0:
                timestr = slog[0].split()[0]
                leave_time = datetime.strptime(timestr, "[%H:%M:%S]")
                return server_time - leave_time
            else:
                timestr = disconnects[-1].split()[0]
                leave_time = datetime.strptime(timestr, "[%H:%M:%S]")
                return server_time - leave_time


if __name__ == "__main__":


    import config


    async def interactive_test( proj= None, inst= None, mc= None,):
        args = input("Write the command you want to run > ")
        method: None | Callable[[], Coroutine[Any, Any, Any]] = None
        try:
            match args.split():
                case ["proj", cmd]:
                    if proj is None:
                        print("No project specified!")
                    else:
                        method = getattr(proj, cmd)
                case["instance", ("get_mc_server" | "start_mc_server") as cmd]:
                    if inst is None:
                        print("No instance specified!")
                    else:
                        mc = await getattr(inst, cmd)(config.rcon_pass, config.rcon_retry_limit)
                        print(mc)
                case ["instance", "ssh_run", *cmd]:
                    if inst is None:
                        print("No instance specified!")
                    else:
                        print (await inst.ssh_run(" ".join(cmd)))
                case ["instance", cmd]:
                    if inst is None:
                        print("No instance specified!")
                    else:
                        method = getattr(inst, cmd)
                case ["mc", "send_rcon", *cmd]:
                    if mc is None:
                        print("No minecraft server specified!")
                    else:
                        print(await mc.send_rcon(" ".join(cmd)))
                case ["mc", cmd]:
                    if mc is None:
                        print("No minecraft server specified!")
                    else:
                        method = getattr(mc, cmd)
                case["exit"]:
                    print("Goodbye!")
                    return 0
                case _:
                    print("Command unrecognized!")
        except AttributeError:
            print("Command unrecognized!")

        if method is not None:
            print(await method())
        await interactive_test(proj, inst, mc)


    async def main():
        proj = Project(id=config.project_id, zone=config.zone)

        i = Instance(
            id=config.instance_id,
            project=proj,
            user=config.user,
            path=config.minecraft_path,
        )

        print( await i.start())

        mc = await i.start_mc_server(config.rcon_pass, config.rcon_retry_limit)

        await interactive_test(proj, i, mc)


    asyncio.run(main())
